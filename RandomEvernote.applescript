tell application "Evernote"
	
	--Set your stacks here.--
	set stackList to {"1. Projects", "2. Areas", "3. Resources"}
	
	--Don't edit below this line.--
	set noteCountTotal to 0
	set noteCountList to {}
	
	--Evernote does not support stack negation in a query.--
	--Please forgive me, oh code gods, for the below workaround.--
	--And if someone wants to make this better, please do!--
	
	repeat with stack from 1 to length of stackList
		set stackName to item stack of stackList
		set noteCountStack to count of (find notes "stack:\"" & stackName & "\"")
		set noteCountTotal to noteCountTotal + noteCountStack
		set end of noteCountList to noteCountStack
	end repeat
	
	set randomNote to random number from 1 to noteCountTotal
	set selectedStackName to ""
	
	repeat with stack from 1 to length of stackList
		set noteCountStack to item stack of noteCountList
		set temp to randomNote - noteCountStack
		if temp is less than 1 then
			set selectedStackName to item stack of stackList
			exit repeat
		else
			set randomNote to randomNote - noteCountStack
		end if
	end repeat
	
	set myNote to item randomNote of (find notes "stack:\"" & selectedStackName & "\"")
	open note window with myNote
	activate
end tell
